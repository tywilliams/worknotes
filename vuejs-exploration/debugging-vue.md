Title: What if you put a debugger on line 7 of Vue.js? 

So we drop `debugger;` on Line 7 of Vue.js

When we load `index.html`, it pauses. In scope, we get a factory function, a global object, and `this`. 

Global and this are just Window. 

If we step through one