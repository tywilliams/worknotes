
# Week ending 2021-08-20

Big ticket item: https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/389
Grad school work

## Monday

- [x] 360 reviews
- [x] Add to one-on-one agenda (every Monday) https://docs.google.com/document/d/1fhcOj6g10SualPDPQQe9cu6zmZRrS12rNHKbTSDieSk/edit

## Tuesday 

- [x] Add Slippers UI to Core Marketing site: https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1465

## Wednesday

- [x] Work on initial CI configuration: https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1466
- [x] Work on taking down staging, but found a team using it for E2E tests:
    - [x] Issue: https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1239
    - Other team: https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/3245
- [x] Work on getting nuxt repo ready for Nathan to get started on enterprise page migration: https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1467#note_654344397
- [x] Create an issue for Javi about making the navigation component configurable: https://gitlab.com/gitlab-com/marketing/inbound-marketing/slippers-ui/-/issues/70
- [x] Schedule time with Chad to work on deployment stuff
- [x] Start prepping for meeting with Chad
    - Comment thread: https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1472#note_656441361
- [x] Add new issue for updating `head`: https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1483

## Thursday

- [x] Add Nuxt content module to new site.
    - MR: https://gitlab.com/gitlab-com/marketing/core-marketing-site/-/merge_requests/5
- [x] Start work on mapping the head over.

## Friday

- [x] Promotion doc
- [x] MR to update API retry: https://gitlab.slack.com/archives/CVDP3HG5V/p1629471801027200?thread_ts=1629432670.027000&cid=CVDP3HG5V. 
    - MR: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/88750
- [x] Keep working on moving the `head` over
