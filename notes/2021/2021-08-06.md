
# Week ending 2021-08-06

## Closing thoughts

Choose a couple: 

* What did you experiment with?
* What was hard?
* What did you enjoy?
* What did you learn?
* Who did you talk to outside of your organisation?
* What would you have liked to do more of?
* What do you wish you could have changed?
* What are you looking forward to next week?

## Monday

- [x] Coffee chat with two team members throughout Marketing

## Tuesday 

- Get started for real planning architecture: https://gitlab.com/gitlab-com/marketing/inbound-marketing/marketing-website/-/issues/161
- MR for data attributes on Google Analytics: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/87650
- Slippers MR to allow comparison infographic to display GitLab scores: https://gitlab.com/gitlab-com/marketing/inbound-marketing/slippers-ui/-/merge_requests/133
- MR for data attributes on navigation: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/87666

## Wednesday

- [x] Interview prep for Javi
- [x] Planning for re-architecture: https://docs.google.com/document/d/1P_5h-efjIXjm-lRVfr1EuSTlsxpMGSqWLZofA43-ai8/edit
- [x] Redo navigation data attributes once the nav changes merge in: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/87666
- [x] More data attributes stuff

## Thursday

- Lots of early meetings
- Javi's interview + follow up
- Data attributes: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/87771#note_644388542

## Friday

- [x] Pivot to work on cache-busting epic: https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/384
    - [x] Add manual cache bust value to `package.json` and documentation
 https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1454
- [x] Add scoring methodology page for comparison pages: https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/5533
    - [x] MR: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/87900
- [ ] Update component: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/87900#note_645330744
    - Issue: https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1456
- [ ] ENG: create a Middleman partial to wrap asset references with cache-busting value: https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1454

## TODO this week (outside of sprint boards)
