
# Week ending 2021-05-28

## Closing thoughts

Choose a couple: 

* What did you experiment with?
* What was hard?
* What did you enjoy?
* What did you learn?
* Who did you talk to outside of your organisation?
* What would you have liked to do more of?
* What do you wish you could have changed?
* What are you looking forward to next week?

## Monday

- [x] OKR: https://gitlab.com/groups/gitlab-com/marketing/strategic-marketing/-/epics/321
- [x] Add to one-on-one agenda (every Monday) https://docs.google.com/document/d/1fhcOj6g10SualPDPQQe9cu6zmZRrS12rNHKbTSDieSk/edit
- [x] Coffee chat with William

## Tuesday 

- [x] Update this epic: https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/340
- [x] Talk to Javi about OKR work
- [x] Ask Barker about why CMS felt like it was out of scope.
- [x] Do OKR planning stuff: https://gitlab.slack.com/archives/CN8AVSFEY/p1620670475108300?thread_ts=1620669142.107700&cid=CN8AVSFEY
- [x] Check one on one async
- [x] Review Javi's Slippers MR: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/82856/diffs?view=parallel#diff-content-0aee9b338ae59aac9dcb911d976a0afcfbc7ff92
- [x] Get started on event template: https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1199

## Wednesday

- [x] Work on event template: https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1199
    - [x] MR: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/83034
- [x] Add to Marketing Strategy and Tactics agenda (every Wednesday) https://docs.google.com/document/d/1VY13aoq2abLlOiGkaSE1KrdCQJcvINjKk8_CRnQP_1M/edit
- [x] Start working on missing navigation component: https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1365
    - [x] MR: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/83052
- [x] Grad school work

## Thursday

- [x] Prep Netlify learning resource: https://docs.google.com/document/d/1PB2s68K4Wtimeai5OYfcjgktjm_tgsZWHRynk9dq-NI/edit
- [x] Check in with Barker and Laura about event template
- [x] Check in with Javi and Tina about comparison pages
- [x] Upload Netlify video
- [x] Finish navigation implementation

## Friday

## TODO this week (outside of sprint boards)

- [ ] Forestry stuff for Shaleen
- [ ] Commit tracing. Vue.js? ActionCable? ActiveRecord? Rails? Something else? Maybe Nuxt?
