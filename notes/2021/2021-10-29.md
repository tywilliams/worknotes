
# Week ending 2021-10-29

Big ticket item: https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/389 
Onboarding buddy: https://gitlab.com/gitlab-com/marketing/digital-experience/engineer-onboarding-for-digital-experience/-/issues/2
Onboarding buddy: https://gitlab.com/gitlab-com/marketing/digital-experience/engineer-onboarding-for-digital-experience/-/issues/4

## Release Notes

1. Wrap gsutil rsync in retry block [Middleman MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/92505) | [Nuxt MR](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/merge_requests/49)
1. Set up conditional recording for LogRocket [Middleman MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/92759) | [Nuxt MR](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/merge_requests/51)

## Monday

- [x] Pair programming with Nathan about Vue.js lifecycle + conventions
- [x] LogRocket conditional recording
    - [x] Create issue. Docs: https://docs.logrocket.com/reference/track
- [x] Add to one-on-one agenda (every Monday) https://docs.google.com/document/d/1fhcOj6g10SualPDPQQe9cu6zmZRrS12rNHKbTSDieSk/edit
- [x] Revisit OKRs

## Tuesday 

- [x] One-on-one
- [x] Conditional recording issue: https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/issues/52
    - Middleman MR: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/92759
    - Nuxt MR: https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/merge_requests/51

## Wednesday

- [x] Fill out OKR survey: https://gitlab.slack.com/archives/G01JH8CA8F3/p1635287045019500
- [x] Follow up on conditional recording LogRocket
- [x] Review: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/92755
- [x] Help Barker with https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/merge_requests/45#note_714866784
- [x] Merge: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/92850

## Thursday

- [x] Merge: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/92897
- [x] Troubleshooting LogRocket conditional recording

## Friday

- [x] More troubleshooting on LogRocket
    - [x] Revert the custom events
- [x] Add dependency caching for www-gitlab-com: https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10232
    - Blocked based on reduced capability in GitLab. I don't think we support npm and RubyGems yet.

## TODO this week (outside of sprint boards)
