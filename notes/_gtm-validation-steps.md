I always end up needing these for MRs:

1. Set `GITLAB_SIMULATE_SAAS=1` in your environment to make GDK [act as SaaS](https://docs.gitlab.com/ee/development/ee_features.html#act-as-saas), since this change only affects SaaS.
1. Create a Google Tag Manager account and container ID. You should just need to [follow the step 1 of the instructions](https://support.google.com/tagmanager/answer/6103696?hl=en). The GTM container can be empty - it just needs to exist.
1. In `gitlab.yml`, add a your GTM ID in the `extra` block for both the `google_tag_manager_id` and `google_tag_manager_nonce_id` values. Make sure to restart GDK (or do this step prior to starting GDK) for settings to take effect.
   ```yml
   extra:
    google_tag_manager_id: 'GTM-YOURIDHERE'
    google_tag_manager_nonce_id: 'GTM-YOURIDHERE'
   ```
1. Start or restart your GDK
1. `bundle exec rails c` to open the Rails console and enable some feature flags:
1. Turn on `Feature.enable(:gitlab_gtm_datalayer)`
1. Open the [Google Tag Assistant](https://tagassistant.google.com/)
1. Visit `http://localhost:3000` from the Tag Assistant

My tag is `GTM-PDDG4LV`

Use `./_gitlab.yml` for a quick config change.
