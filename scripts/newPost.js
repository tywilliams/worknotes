const fs = require('fs')
const path = require('path');

newPost();

function newPost() {
    const filePath = generateFilePath();
    const content = generateContent();
    fs.writeFile(filePath, content, { flag: 'w+' }, (err) => {
        if (err) return console.log(err);
        console.log(`Created ${filePath}`);
    });
}

function generateFilePath() {
    return path.join(__dirname, '..', 'notes', `${nextFriday()}.md`);
}

// StackOverflow, Calculating the next day of the week based on today's date: 
// https://codereview.stackexchange.com/a/33532
// Since we only care about the next Friday from the day we run the script,
// we can simplify it and hardcode the numbers a little.
function nextFriday() {
    const resultDate = new Date();
    resultDate.setDate(resultDate.getDate() + (12 - resultDate.getDay()) % 7);
    const isoDateWithoutTimeStamp = resultDate.toISOString().split('T')[0]
    return isoDateWithoutTimeStamp;
}

function generateContent() {
    return `
# Week ending ${nextFriday()}

## Release Notes

## Monday

## Tuesday 

## Wednesday

## Thursday

## Friday

## TODO this week (outside of sprint boards)

- [ ] Add to one-on-one agenda (every Monday) https://docs.google.com/document/d/1fhcOj6g10SualPDPQQe9cu6zmZRrS12rNHKbTSDieSk/edit
- [ ] Add to sprint release agenda (every other Wednesday) https://docs.google.com/document/d/1I9Th3Q-AakOkE_-pmNtEzwwMDSqYKF5Je2etGdPTovk/edit
- [ ] Add to sprint retro agenda (every other Wednesday) https://docs.google.com/document/d/1kMNiUF2UDuSrMDuzLyRi8OEhVxry_MJoYi38RmmWafY/edit#

`
}