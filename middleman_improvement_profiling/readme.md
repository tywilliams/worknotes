# Quantifying Middleman Performance Improvements

Last week, I wrote a quick monkeypatch to [improve the speed of our local Middleman preview server](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/73487). Here's a breakdown of how much faster it really is: 

## Steps to reproduce

I ran these steps on my local computer, a MacBook Pro (13-inch, 2020), with Big Sur Version 11.2. 2.3 GHz Quad-Core Intel Core i7 32 GB 3733 MHz LPDDR4X RAM.

1. Check out the master branch of [`www-gitlab-com`](https://gitlab.com/gitlab-com/www-gitlab-com)
1. I had the `public` directory built, which may have an impact (Middleman preview server will try to serve existing files if they have already been built).
1. `cd sites/marketing`
1. Run `NO_CONTRACTS=true nohup bundle exec middleman serve --instrument`
1. Change markup in `sites/marketing/source/includes/cms/topic/body.html.haml` and time the change
1. Change frontmatter in `sites/marketing/source/templates/topic_detailed.html.haml` and time the change
1. The output of that series of events is available in `rebuild_enabled.txt`
1. Shut down server
1. Run `DISABLE_ON_DISK_REBUILD=true NO_CONTRACTS=true nohup bundle exec middleman serve --instrument`
1. Change markup in `sites/marketing/source/includes/cms/topic/body.html.haml` and time the change
1. Change frontmatter in `sites/marketing/source/templates/topic_detailed.html.haml` and time the change
1. The output of that series of events is available in `rebuild_disabled.txt`

I pulled the data into [this spreadsheet](https://docs.google.com/spreadsheets/d/1gpqEcmaBOWr9yH9XERBsFOchR-gbgreirVTbcnQ03EM/edit?usp=sharing) and removed the `==` characters because Sheets was reading it as a formula. 

Then I split out the milliseconds, added them up, and converted to seconds. 

In total, the time spent waiting for local changes by default was about 3 minutes. With our patches, it was 46 seconds. That's almost 4x faster:

![Bar chart comparing the old server and monkeypatched server](middleman_improvement_profiling/benchmark-chart.png)
